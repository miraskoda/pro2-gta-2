package cz.uhk.fim.pro2.gta.model;

import java.util.ArrayList;
import java.util.List;

public class World {

	private List<Heart> hearts;
	private Car car;
	private List<Box> boxes;
	
	public World(Car car) {
		this.car = car;
		this.hearts = new ArrayList<>();
		this.boxes = new ArrayList<>();
	}
	
	
	
	public void addBox(Box box){
		boxes.add(box);
	}
	
	
	public void addHeart(Heart heart) {
		hearts.add(heart);
	}
	
	
	
}
