package cz.uhk.fim.pro2.gta.model;

public class Car extends GameObject{
	
	private String name;
	
	
	public Car(String name, float x, float y) {
		super(x,y);
		this.name=name;
	}

}
