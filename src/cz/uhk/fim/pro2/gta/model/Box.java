package cz.uhk.fim.pro2.gta.model;

public class Box extends GameObject{

	private int width;
	
	public Box(int width, float x, float y) {
		super(x, y);
		this.width=width;
		
	}
	
	
	public Box(float x, float y) {
		this(1, x, y);
	}
	
}
