package cz.uhk.fim.pro2.gta;

import cz.uhk.fim.pro2.gta.model.*;

import javafx.scene.shape.Box;

public class App {

	public static void main(String[] args) {

	Car car = new Car("Hyundai i30", 10f, 20f);
			
	World world = new World(car);	
	
	world.addBox(new Box(5f, 10f));
	world.addBox(new Box(2, 10f, 20f));

	world.addHeart(new Heart(10f,30f));
	world.addHeart(new Heart(30.4f, 22.2f));
	
		
	
	
	}

}
